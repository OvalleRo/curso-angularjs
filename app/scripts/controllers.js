'use strict';
angular.module('videostore').controller('HomeController', ['$scope', 'filmService', function($scope, filmService){
  $scope.showDetails = false;
  filmService.getFilms().then(
    function(response){
      $scope.films = response.data
    }
    );

  $scope.toggleDetails = function(){
    $scope.showDetails = !$scope.showDetails;
  };
}]);

angular.module('videostore').controller('HeaderController', ['$scope', '$location',
  function($scope, $location){
    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
  }
  ]);
