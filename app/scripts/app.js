'use strict';

var app = angular.module('videostore', ['ui.router']);

app.config(Config)
Config.$inject = ['$stateProvider', '$urlRouterProvider']

function Config ($stateProvider, $urlRouterProvider) {
     $stateProvider.state('app', {
          url:'/',
          views: {
               'header': {
                    templateUrl : 'header.html'
               },
               'content': {
                    templateUrl : 'filmList.html',
                    controller : 'HomeController'
               }
          }
     });
     $stateProvider.state('contact', {
          url: '/contact',
          views: {
               'header': {
                    templateUrl : 'header.html'
               },
               'content': {
                    templateUrl : 'contactus.new.html',
                    controller : 'FeedbackController'
               }
          }

     });
     $urlRouterProvider.otherwise('/');
}

app.constant("baseURL", "http://localhost:3000/")

app.controller('ContactController', ContactController);
app.controller('FeedbackController', FeedbackController);

ContactController.$inject = ['$scope'];
function ContactController($scope) {
     var channels = [{
          value: "tel",
          label: "Tel."
     },
     {
          value: "Email",
          label: "Email"
     }];
     $scope.channels = channels;
     $scope.invalidChannelSelection = false;
}

FeedbackController.$inject = ['$scope'];
function FeedbackController($scope) {
     $scope.feedback = {
          mychannel: "",
          firstname: "",
          lastname: "",
          agree: false,
          email: ""
     };
     $scope.sendFeedback = function(){
          $scope.showMessage = false;
          if ($scope.feedback.agree && ($scope.feedback.mychannel == "" || !$scope.feedback.mychannel)){
               $scope.invalidChannelSelection = true;
          } else {
               $scope.invalidChannelSelection = false;
               $scope.feedback.mychannel = "";
               $scope.feedback = {
                    mychannel: "",
                    firstname: "",
                    lastname: "",
                    agree: false,
                    email: ""
               };
               $scope.feedbackForm.$setPristine();
               $scope.showMessage = true;
          }
     };
}
