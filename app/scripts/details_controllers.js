var app = angular.module('videostore',[]);

app.controller('filmDetailController', filmDetailController);
filmDetailController.$inject=['$scope', 'filmDetailsService'];
function filmDetailController($scope, filmDetailsService) {

 $scope.film = filmDetailsService.getFilm();

}


app.controller('FilmCommentController', FilmCommentController);
FilmCommentController.$inject=['$scope', 'filmDetailsService'];
function FilmCommentController($scope, filmDetailsService) {
  $scope.submitComment = function() {
    var filmComment = {
      rating: $scope.comment.rating,
      comment: $scope.comment.cmnt,
      author: $scope.comment.userName,
      date: ""
    };
    filmComment.date = new Date().toISOString();
    $scope.film = filmDetailsService.getFilm();
    $scope.film.comments.push(filmComment);
    $scope.cmntForm.$setPristine();
  }
}
