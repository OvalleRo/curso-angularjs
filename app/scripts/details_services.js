'use strict';
angular.module('videostore').service('filmDetailsService', function(){

  var film={
   title:'ACADEMY DINOSAUR',
   image: 'images/academy_dinosaur.jpg',
   length: '86',
   price:'4.99',
   release: '2006',
   description:'A Epic Drama of a Feminist And a Mad Scientist who must Battle a Teacher in The Canadian Rockies.',
   comments: [
   {
     rating:5,
     comment:"Excelente Pelicula!!",
     author:"John Lemon",
     date:"2017-01-01T17:57:28.556094Z"
   },
   {
     rating:4,
     comment:"Muy recomendada!",
     author:"Paul McVites",
     date:"2017-01-05T17:57:28.556094Z"
   },
   {
     rating:3,
     comment:"Entretenida pero por momentos aburrida",
     author:"Michael Jaikishan",
     date:"2017-01-13T17:57:28.556094Z"
   },
   {
     rating:4,
     comment:"Muy buena!!",
     author:"Ringo Starry",
     date:"2017-01-02T17:57:28.556094Z"
   },
   {
     rating:2,
     comment:"Muy lenta",
     author:"25 Cent",
     date:"2017-01-02T17:57:28.556094Z"
   }

   ]
 };
 function getFilm() { return film; }
 return({
  getFilm: getFilm
 });
});
